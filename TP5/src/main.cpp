/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 26 fevrier 2021
\brief fichier principal du programme qui test la class Robot
*/

#include <vector>
#include <algorithm>
#include <fstream>
#include "Robot.h"
using namespace std;

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief la fonction principale qui test la class Robot
\return 0 si tout c'est bien passé
*/
int main(){
    //declaration du vecteur qui sauvegarde les robots
    vector<Robot> robots;

    // Lecture du fichier Robots.txt
    ifstream fluxLectureRobots("./files/Robots.txt", ios::in);
    Robot robotCourant; // Le robot courant lors de la lecture du fichier
    if(fluxLectureRobots) {
        while(!fluxLectureRobots.eof()) {
            fluxLectureRobots >> robotCourant;
            if (!fluxLectureRobots.fail()) {
                robots.push_back(robotCourant);
            }
        }
    }
    fluxLectureRobots.close();

    // afficher les robots
    cout << "Robots :" << endl;
    for (auto& robot : robots) {
        cout << "\t" << robot << endl;
    }

    // chercher si "Wall-e" est dans les robots
    auto pos = find_if(robots.begin(),
                       robots.end(),
                       [](const Robot& robot){
                            return robot.getName() == "Wall-e";
                        });
    if ( pos != robots.end() )
        cout << endl << "Wall-e est présent dans la liste ! " << endl;
    else
        cout << endl << "Wall-e n'est pas dans la liste... :'( " << endl;

    // trier les robots par prix
    sort(robots.begin(),
         robots.end());

    // afficher les robots trié
    cout << endl << "Robots triés par prix :" << endl;
    for (auto& robot : robots) {
        cout << "\t" << robot << endl;
    }

    // sauvegarder les robots trié dans un nouveau fichier SortedRobots.txt
    ofstream fluxEcritureRobots("./files/SortedRobots.txt", ios::out);
    if(fluxEcritureRobots) {
        for (auto& robot : robots) {
            fluxEcritureRobots << robot << endl;
        }
    }
    fluxEcritureRobots.close();

    return(0);
}