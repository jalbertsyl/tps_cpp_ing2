/*!
\file Robot.cpp
\autor Jalbert Sylvain
\version 1
\date 26 fevrier 2021
\brief fichier qui défini les fonctions de la classe Robot
*/

#include "Robot.h"


/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief Constructeur par défaut de Robot
*/
Robot::Robot(){
    Robot("no name", "no type", -1.0);
}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief Constructeur de Robot
*/
Robot::Robot(const std::string & n){
    Robot(n, "no type", -1.0);
}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief Constructeur de Robot
*/
Robot::Robot(const std::string & n, const std::string & t, double p)
    : name{n}
    , type{t}
    , price{p}{}

/*!
\fn std::string getName() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction qui permet de d'obtenir le nom du robot
\return le nom du robot
*/
std::string Robot::getName() const{
    return this->name;
}

/*!
\fn std::string getType() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction qui permet de d'obtenir le type du robot
\return le type du robot
*/
std::string Robot::getType() const{
    return this->type;
}

/*!
\fn double getPrice() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction qui permet de d'obtenir le prix du robot
\return le prix du robot
*/
double Robot::getPrice() const{
    return this->price;
}

/*!
\fn void setName(const std::string n)
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief méthode qui permet de changer le nom du robot
\param n le nouveau nom
*/
void Robot::setName(const std::string n){
    this->name = n;
}

/*!
\fn void setType(const std::string t)
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief méthode qui permet de changer le type du robot
\param t le nouveau type
*/
void Robot::setType(const std::string t){
    this->type = t;
}

/*!
\fn void setPrice(const double p)
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief méthode qui permet de changer le prix du robot
\param p le nouveau prix
*/
void Robot::setPrice(const double p){
    this->price = p;
}

/*!
\fn bool operator<(const Robot & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction qui implemente l'opérateur < à Robot
\param other le Robot à comparer
\return vrai si le prix du robot courant est inferieur à celui de other, faux sinon
*/
bool Robot::operator<(const Robot & other) const{
    return(this->getPrice() < other.getPrice());
}

/*!
\fn friend std::ostream & operator<<(std::ostream & out, const Robot & p)
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction amie qui permet d'ajouter à une sortie ostream, le Robot sous forme de text
\param out la sortie ostream de base
\param p le Robot à ajouter à la sortie
\return la nouvelle sortie ostream modifié
*/
std::ostream & operator<<(std::ostream & out, const Robot & p){
    return(out << p.getName() << " " << p.getType() << " " << p.getPrice());
}

/*!
\fn friend std::istream & operator>>(std::istream & in, Robot & p)
\author Jalbert Sylvain
\version 1 Premier jet
\date 26 fevrier 2021
\brief fonction amie qui permet de donner à une entrée istream les variables du Robot a modifier
\param in l'entrée istream de base
\param p le Robot qui sera modifié avec les valeurs donné
\return la nouvelle entrée istream modifié
*/
std::istream & operator>>(std::istream & in, Robot & p){
    if(!(in >> p.name >> p.type >> p.price )){
        in.setstate( std::ios::failbit ) ;
    }
    return in;
}