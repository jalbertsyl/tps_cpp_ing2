/*!
\file Game.cpp
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui défini les fonctions de la classe Game
*/

#include "Game.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief Constructeur par défault de Game
\details Initialise les tours et les disques
*/
Game::Game():myTowers(new Tower[3]){
    this->myTowers[0].setName("départ");
    this->myTowers[1].setName("intermédiaire");
    this->myTowers[2].setName("arrivée");

    for(int i = 4 ; i > 0 ; i--){
        Disk * newDisk = new Disk(i);
        this->myTowers[0].pushDisk(*newDisk);
    }
}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief Destructeur de Game
\details Delete les tours
*/
Game::~Game(){
    delete[] this->myTowers;
}

/*!
\fn void solve()
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief procédure qui résoud le jeu
*/
void Game::solve(){
    //clear la console
    printf("\033c");
    //afficher l'état de la partie au début
    cout << "Etat initial :" << endl << *this << endl;
    //attendre 1,5 seconde
    usleep(1500000);
    //clear la console
    printf("\033c");
    //résoudre la partie
    this->towerOfHanoi(4, 0, 2, 1);
    //afficher l'état de la partie à la fin
    cout << "Etat final :" << endl << *this << endl;
}


/*!
\fn void towerOfHanoi(int n, int startTowerNum, int endTowerNum, int interTowerNum)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief procédure qui résoud le jeu de hanoi de façon récursive
\param n le nombre de disk restant à ce tour
\param startTowerNum le numéro de la tour de où on extrait le disque à ce tour
\param endTowerNum le numéro de la tour de où on ajoute le disque à ce tour
\param startTowerNum le numéro de la tour qui servira d'intermédiaire à ce tour
*/
void Game::towerOfHanoi(int n, int startTowerNum, int endTowerNum, int interTowerNum) {
    if(n == 1){
        // Bouger le disque de la tour numéro startTowerNum à la tour numéro endTowerNum
        this->myTowers[endTowerNum].pushDisk( this->myTowers[startTowerNum].popDisk() );
        //afficher l'état de la partie à cet étape
        cout << "Tour suivant (tour n°" << startTowerNum + 1 << " vers n°" << endTowerNum + 1 << ") :" << endl << *this << endl << endl;
        //attendre 1 seconde
        usleep(1000000);
        //clear la console
        printf("\033c");
        return; 
    }
    towerOfHanoi(n - 1, startTowerNum, interTowerNum, endTowerNum);
    // Bouger le disque de la tour numéro startTowerNum à la tour numéro endTowerNum
    this->myTowers[endTowerNum].pushDisk( this->myTowers[startTowerNum].popDisk() );
    //afficher l'état de la partie à cet étape
    cout << "Tour suivant (tour n°" << startTowerNum + 1 << " vers n°" << endTowerNum + 1 << ") :" << endl << *this << endl << endl;
    //attendre 1 seconde
    usleep(1000000);
    //clear la console
    printf("\033c");
    towerOfHanoi(n - 1, interTowerNum, endTowerNum, startTowerNum);
}

/*!
\fn std::ostream& operator<<(std::ostream& out, const Game & g)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction amie qui permet d'ajouter à une sortie ostream, la Game à l'état actuel sous forme de text
\param out la sortie ostream de base
\param g la Game à ajouter à la sortie
\return la nouvelle sortie ostream modifié
*/
std::ostream& operator<<(std::ostream& out, const Game & g){
    stack<Disk> saveDiskOfTowers[]{g.myTowers[0].getMyDisks(), g.myTowers[1].getMyDisks(), g.myTowers[2].getMyDisks()};
    //parcour de toutes les lignes des tours
    for(unsigned int i = 4 ; i >= 1 ; i--){
        //parcour des 3 tours
        for(int j = 0 ; j < 3 ; j++){
            if(g.myTowers[j].getMyDisks().size() < i){
                out << "              "; // ajout de 14 espaces
            } else {
                //afficher le disque
                out << "   " << saveDiskOfTowers[j].top() << "   ";
                saveDiskOfTowers[j].pop();
            }
        }
        //aller à la ligne
        out << endl;
    }
    // nom des tours
    out << "|_____________|_____________|_____________|" << endl;
    out << "|      1      |      2      |      3      |" << endl;
    out << "|   " << g.myTowers[0].getName() <<
           "    |" << g.myTowers[1].getName() <<
           "|   " << g.myTowers[2].getName() << "   |" << endl << endl;
    return(out);
}