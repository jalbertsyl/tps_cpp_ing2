/*!
\file Tower.cpp
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui défini les fonctions de la classe Tower
*/

#include "Tower.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief Constructeur par défaut de Tower
\details Créer une tower sans nom et sans disk
*/
Tower::Tower(){
    Tower("");
}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief Constructeur de Tower
\details Créer une tower avec un nom donné et sans disk
\param n nom de la tour
*/
Tower::Tower(string n)
    :name{n}{}

/*!
\fn void pushDisk(Disk d)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief procédure qui permet d'inserer un disque dans la tour
\param d le disque a inserer
*/
void Tower::pushDisk(Disk& d){
    this->myDisks.push(d);
}

/*!
\fn Disk popDisk()
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction qui permet de retiré le disque en haut de la tour
\return le disque retiré
*/
Disk& Tower::popDisk(){
    Disk * diskPoped = &this->myDisks.top();
    this->myDisks.pop();
    return *diskPoped;
}

/*!
\fn string getName()
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction qui renvoie le nom de la tour
\return le nom de la tour
*/
string Tower::getName(){
    return this->name;
}

/*!
\fn void setName(string n)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief procédure qui permet de renomer la tour
\param n le nouveau nom
*/
void Tower::setName(string n){
    this->name = n;
}

/*!
\fn stack<Disk> getMyDisks()
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction qui renvoie la pile de disque de la tour
\return la pile de disque de la tour
*/
stack<Disk> Tower::getMyDisks(){
    return this->myDisks;
}