/*!
\file Disk.cpp
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui défini les fonctions de la classe Disk
*/

#include "Disk.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief Constructeur de Disk
\details Initialise le disk avec le diametre donné en parametre
\param d Le diametre du disk
*/
Disk::Disk(int d):diameter{d}{}

/*!
\fn int getDiameter()
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction qui renvoie le dimetre du disk
\return le dimetre du disk
*/
int Disk::getDiameter(){
    return this->diameter;
}
/*!
\fn std::ostream& operator<<(std::ostream& out, const Disk & d)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction amie qui permet d'ajouter à une sortie ostream, le Disk sous forme de text
\param out la sortie ostream de base
\param d le disque à ajouter à la sortie
\return la nouvelle sortie ostream modifié
*/
std::ostream& operator<<(std::ostream& out, const Disk & d){
    for(int i = 0 ; i < d.diameter ; i++){
        out << "_";
    }
    //combler le vide
    for(int i = 8 ; i > d.diameter ; i--){
        out << " ";
    }
    return(out);
}