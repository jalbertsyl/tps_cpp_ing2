/*!
\file Tower.h
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui déclare la classe Tower
*/

#ifndef __TOWER_H_
#define __TOWER_H_

#include <stack>
#include <iostream>
using namespace std;
#include "Disk.h"

class Tower {
    private :
        string name;
        stack<Disk> myDisks;
    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief Constructeur par défaut de Tower
        \details Créer une tower sans nom et sans disk
        */
        Tower();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief Constructeur de Tower
        \details Créer une tower avec un nom donné et sans disk
        \param n nom de la tour
        */
        Tower(string n);

        /*!
        \fn void pushDisk(Disk d)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief procédure qui permet d'inserer un disque dans la tour
        \param d le disque a inserer
        */
        void pushDisk(Disk& d);

        /*!
        \fn Disk popDisk()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction qui permet de retiré le disque en haut de la tour
        \return le disque retiré
        */
        Disk& popDisk();

        /*!
        \fn string getName()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction qui renvoie le nom de la tour
        \return le nom de la tour
        */
        string getName();

        /*!
        \fn void setName(string n)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief procédure qui permet de renomer la tour
        \param n le nouveau nom
        */
        void setName(string n);

        /*!
        \fn stack<Disk> getMyDisks()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction qui renvoie la pile de disque de la tour
        \return la pile de disque de la tour
        */
        stack<Disk> getMyDisks();
} ;
#endif