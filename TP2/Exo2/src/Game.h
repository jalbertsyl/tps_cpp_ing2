/*!
\file Game.h
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui déclare la classe Game
*/

#ifndef __GAME_H_
#define __GAME_H_

//#include <windows.h>
#include<unistd.h>

#include "Tower.h"


class Game {
    private :
        Tower * myTowers;
    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief Constructeur par défault de Game
        \details Initialise les tours et les disques
        */
        Game();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief Destructeur de Game
        \details Delete les tours
        */
        ~Game();

        /*!
        \fn void solve()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief procédure qui résoud le jeu
        */
        void solve();

        /*!
        \fn void towerOfHanoi(int n, int startTowerNum, int endTowerNum, int interTowerNum)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief procédure qui résoud le jeu de hanoi de façon récursive
        \param n le nombre de disk restant à ce tour
        \param startTowerNum le numéro de la tour de où on extrait le disque à ce tour
        \param endTowerNum le numéro de la tour de où on ajoute le disque à ce tour
        \param startTowerNum le numéro de la tour qui servira d'intermédiaire à ce tour
        */
        void towerOfHanoi(int n, int startTowerNum, int endTowerNum, int interTowerNum);

        /*!
        \fn std::ostream& operator<<(std::ostream& out, const Game & g)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction amie qui permet d'ajouter à une sortie ostream, la Game à l'état actuel sous forme de text
        \param out la sortie ostream de base
        \param g la Game à ajouter à la sortie
        \return la nouvelle sortie ostream modifié
        */
        friend std::ostream& operator<<(std::ostream& out, const Game & g);
} ;
#endif