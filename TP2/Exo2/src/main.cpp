/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier principal du programme créer et lance la partie des tours de Hanoï
*/

#include "Game.h"

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief la fonction principale qui créer et lance la partie des tours de Hanoï
\return 0 si tout c'est bien passé
*/
int main(){
    Game * maPartie = new Game();
    maPartie->solve();
    delete maPartie;
    return(0);
}