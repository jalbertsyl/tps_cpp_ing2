/*!
\file Disk.h
\autor Jalbert Sylvain
\version 1
\date 22 janvier 2021
\brief fichier qui déclare la classe Disk
*/

#ifndef __DISK_H_
#define __DISK_H_

#include <iostream>
using namespace std;

class Disk {
    private :
        int diameter;
    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief Constructeur de Disk
        \details Initialise le disk avec le diametre donné en parametre
        \param d Le diametre du disk
        */
        Disk(int d);

        /*!
        \fn int getDiameter()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction qui renvoie le dimetre du disk
        \return le dimetre du disk
        */
        int getDiameter();

        /*!
        \fn std::ostream& operator<<(std::ostream& out, const Disk & d)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 22 janvier 2021
        \brief fonction amie qui permet d'ajouter à une sortie ostream, le Disk sous forme de text
        \param out la sortie ostream de base
        \param d le disque à ajouter à la sortie
        \return la nouvelle sortie ostream modifié
        */
        friend std::ostream& operator<<(std::ostream& out, const Disk & d);
} ;
#endif