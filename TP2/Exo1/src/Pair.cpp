#include "Pair.hpp"

Pair::Pair(int a, int b)
    : pa(new int(a))
    , pb(new int(b)){}

Pair::Pair(const Pair & other)
    : Pair(*other.pa, *other.pb){}

Pair::~Pair(){
    delete pa;
    delete pb;
}

std::ostream& operator<<(std::ostream& out, const Pair & p){
    return out << *p.pa << " " << *p.pb;
}