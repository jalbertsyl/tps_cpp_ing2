/*!
\file Directory.cpp
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui défini les fonctions de la classe Directory
*/

#include "Directory.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur par défaut de Directory
*/
Directory::Directory()
    : name{"Sans nom"}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur de Directory
*/
Directory::Directory (string name)
    : name{name}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Destructeur de Directory
*/
Directory::~Directory (){
    for (auto& file : this->getFiles()){
        delete file;
    }
}

/*!
\fn string getName() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir le nom du répertoire
\return le nom du répertoire
*/
string Directory::getName() const{
    return this->name;
}

/*!
\fn void setName(string newName)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir un nouveau nom au répertoire
\param newName le nouveau nom du répertoire
*/
void Directory::setName(string newName){
    this->name = newName;
}

/*!
\fn vector<File *> getFiles() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir les fichiers présent dans le répertoire
\return le vecteur de fichiers
*/
vector<File *> Directory::getFiles() const{
    return this->files;
}

/*!
\fn void addFile(File * newFile)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet d'ajouter un fichier au répertoire
\param newFile le nouveau fichier à ajouter
*/
void Directory::addFile(File * newFile){
    this->files.push_back(newFile);
}

/*!
\fn ostream & operator<<(ostream & out, const Directory & d)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction amie qui permet d'ajouter à une sortie ostream, le Directory sous forme de text
\param out la sortie ostream de base
\param d le Directory à ajouter à la sortie
\return la nouvelle sortie ostream modifié
*/
ostream & operator<<(ostream & out, const Directory & d){
    //afficher le nom du répertoire
    out << d.getName() << " :" << "\t\tsize :" << "\tcompressed size :\n";
    // ajouter chaque fichier à la sortie
    for (auto& file : d.getFiles()){
        //afficher le nom et l'extension
        out << "\t" << file->getName() << file->getExtension();
        //calculer le nombre de lettre dans le nom et l'extension du fichier
        unsigned int lengthName = file->getName().length() + file->getExtension().length();
        if(lengthName < 20){
            for(unsigned int i = 20 ; i > lengthName ; i--){
                out << " ";
            }
        }
        out << "\t" << file->getSize() << "\t" << file->getCompressedSize() << "\n";
    }
    return(out);
}