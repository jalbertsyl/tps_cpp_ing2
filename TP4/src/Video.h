/*!
\file Video.h
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui déclare la classe Video
*/

#ifndef __VIDEO_H_
#define __VIDEO_H_

#include "File.h"

class Video : public File {
    private :
        unsigned int duration;

    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur par défaut de File
        */
        Video();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur de File
        */
        Video(string name, string extension, unsigned int size, unsigned int duration);

        /*!
        \fn unsigned int getDuration() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir la durée de la vidéo
        \return la durée de la vidéo
        */
        unsigned int getDuration() const;

        /*!
        \fn void setDuration(unsigned int newDuration)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir une nouvelle durée de vidéo
        \param newDuration la nouvelle durée de vidéo
        */
        void setDuration(unsigned int newDuration);

        /*!
        \fn unsigned int getCompressedSize() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet d'avoir la taille du fichier une fois compréssé
        \return la taille du fichier une fois compréssé
        */
        unsigned int getCompressedSize() const;
} ;


#endif