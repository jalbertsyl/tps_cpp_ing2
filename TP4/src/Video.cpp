/*!
\file Video.cpp
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui défini les fonctions de la classe Video
*/

#include "Video.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur par défaut de Video
*/
Video::Video()
     : duration{0}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur de File
*/
Video::Video(string name, string extension, unsigned int size, unsigned int duration)
     : File(name, extension, size)
     , duration{duration}{}

/*!
\fn unsigned int getDuration() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir la durée de la vidéo
\return la durée de la vidéo
*/
unsigned int Video::getDuration() const{
    return this->duration;
}

/*!
\fn void setDuration(unsigned int newDuration)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir une nouvelle durée de vidéo
\param newDuration la nouvelle durée de vidéo
*/
void Video::setDuration(unsigned int newDuration){
    this->duration = newDuration;
}

/*!
\fn unsigned int getCompressedSize() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet d'avoir la taille du fichier une fois compréssé
\return la taille du fichier une fois compréssé
*/
unsigned int Video::getCompressedSize() const{
    //la taille du fichier compressé sera 80% de la durée de la vidéo.
    return this->getDuration() * 0.8;
}