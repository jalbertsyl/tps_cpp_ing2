/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 29 janvier 2021
\brief fichier principal du programme qui test la class Time
*/
#include <typeinfo>

#include "Directory.h"
#include "Text.h"
#include "Image.h"
#include "Video.h"

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief la fonction principale qui test la class Time
\return 0 si tout c'est bien passé
*/
int main(){
    //contiendra le taux moyen de compression des fichiers vidéos dans le dossier
    double tauxMoyenCompressionVideo = 0.0;
    //compte le nombre de video dans le dossier
    unsigned int nombreDeVideo = 0;

    // Création du jeu de données
    Directory * dossier = new Directory("Mon répertoire");
    dossier->addFile(new Image( "PetitPoney",     ".png",  752, 10, 20));
    dossier->addFile(new Text ( "mesNotes",       ".txt",  200, ASCII));
    dossier->addFile(new Image( "Image",          ".jpg",  800, 28, 28));
    dossier->addFile(new Video( "annivBenoit",    ".avi",  100, 50));
    dossier->addFile(new Image( "photoNoel",      ".png",  736, 10, 20));
    dossier->addFile(new Text ( "lettres",        ".txt",  350, Unicode));
    dossier->addFile(new Text ( "monRoman",       ".txt",  500, Unicode));
    dossier->addFile(new Video( "noel2014",       ".mp4",  1000, 600));
    dossier->addFile(new Image( "PhotoNaissance", ".gif",  500, 15, 20));


    // afficher le dossier
    cout << *dossier << endl;

    //calculer le taux moyen de compression des fichiers vidéos dans le dossier
    for (auto& file : dossier->getFiles()){
        if(typeid(*file) == typeid(Video)){
            tauxMoyenCompressionVideo += 1 - (double)file->getCompressedSize() / (double)file->getSize();
            nombreDeVideo++;
        }
    }
    tauxMoyenCompressionVideo /= nombreDeVideo;
    //afficher le résultat
    cout << "Le taux moyen de compression des fichiers vidéos dans le dossier est de : " << tauxMoyenCompressionVideo * 100 << "%" << endl;

    // liberer espace mémoire
    delete dossier;

    return(0);
}