/*!
\file File.h
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui déclare la classe File
*/

#ifndef __FILE_H_
#define __FILE_H_

#include <iostream>
using namespace std;

class File {
    private :
        string name;
        string extension;
        unsigned int size;


    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur par défaut de File
        */
        File();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur de File
        */
        File(string name, string extension, unsigned int size);

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Destructeur de File
        */
        virtual ~File ();

        /*!
        \fn string getName() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir le nom du fichier
        \return le nom du fichier
        */
        string getName() const;

        /*!
        \fn void setName(string newName)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir un nouveau nom au fichier
        \param newName le nouveau nom du fichier
        */
        void setName(string newName);

        /*!
        \fn string getExtension() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir l'extension du fichier
        \return l'extension du fichier
        */
        string getExtension() const;

        /*!
        \fn void setExtension(string newExtension)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir une nouvelle extension de fichier
        \param newExtension la nouvelle extension du fichier
        */
        void setExtension(string newExtension);

        /*!
        \fn unsigned int getSize() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir la taille du fichier
        \return la taille du fichier
        */
        unsigned int getSize() const;

        /*!
        \fn void setSize(unsigned int newSize)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir une nouvelle taille de fichier
        \param newSize la nouvelle taille du fichier
        */
        void setSize(unsigned int newSize);

        /*!
        \fn virtual unsigned int getCompressedSize() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet d'avoir la taille du fichier une fois compréssé
        \return la taille du fichier une fois compréssé
        */
        virtual unsigned int getCompressedSize() const = 0;
} ;


#endif