/*!
\file Text.h
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui déclare la classe Text
*/

#ifndef __TEXT_H_
#define __TEXT_H_

#include "File.h"

enum Encoding { ASCII, Unicode };

class Text : public File {
    private :
        Encoding encoding;

    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur par défaut de File
        */
        Text();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur de File
        */
        Text(string name, string extension, unsigned int size, Encoding encoding);

        /*!
        \fn Encoding getEncoding() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir le type d'encodage du fichier
        \return le type d'encodage du fichier
        */
        Encoding getEncoding() const;

        /*!
        \fn void setEncoding(Encoding newEncoding)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir un nouveau type d'encodage du fichier
        \param newEncoding le nouveau type d'encodage du fichier
        */
        void setEncoding(Encoding newEncoding);

        /*!
        \fn unsigned int getCompressedSize() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet d'avoir la taille du fichier une fois compréssé
        \return la taille du fichier une fois compréssé
        */
        unsigned int getCompressedSize() const;
} ;


#endif