/*!
\file Image.h
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui déclare la classe Image
*/

#ifndef __IMAGE_H_
#define __IMAGE_H_

#include "File.h"

class Image : public File {
    private :
        unsigned int width;
        unsigned int height;

    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur par défaut de File
        */
        Image();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur de File
        */
        Image(string name, string extension, unsigned int size, unsigned int width, unsigned int height);

        /*!
        \fn unsigned int getWidth() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir la largeur de l'image
        \return la largeur de l'image
        */
        unsigned int getWidth() const;

        /*!
        \fn void setWidth(unsigned int newWidth)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir une nouvelle largeur d'image
        \param newWidth la nouvelle largeur de l'image
        */
        void setWidth(unsigned int newWidth);

        /*!
        \fn unsigned int getHeight() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir la hauteur de l'image
        \return la hauteur de l'image
        */
        unsigned int getHeight() const;

        /*!
        \fn void setHeight(unsigned int newHeight)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir une nouvelle hauteur d'image
        \param newHeight la nouvelle hauteur de l'image
        */
        void setHeight(unsigned int newHeight);

        /*!
        \fn unsigned int getCompressedSize() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet d'avoir la taille du fichier une fois compréssé
        \return la taille du fichier une fois compréssé
        */
        unsigned int getCompressedSize() const;
} ;


#endif