/*!
\file Text.cpp
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui défini les fonctions de la classe Text
*/

#include "Text.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur par défaut de Text
*/
Text::Text()
    : encoding{ASCII}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur de File
*/
Text::Text(string name, string extension, unsigned int size, Encoding encoding)
    : File(name, extension, size)
    , encoding{encoding}{}

/*!
\fn Encoding getEncoding() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir le type d'encodage du fichier
\return le type d'encodage du fichier
*/
Encoding Text::getEncoding() const{
    return this->encoding;
}

/*!
\fn void setEncoding(Encoding newEncoding)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir un nouveau type d'encodage du fichier
\param newEncoding le nouveau type d'encodage du fichier
*/
void Text::setEncoding(Encoding newEncoding){
    this->encoding = newEncoding;
}

/*!
\fn unsigned int getCompressedSize() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet d'avoir la taille du fichier une fois compréssé
\return la taille du fichier une fois compréssé
*/
unsigned int Text::getCompressedSize() const{
    //si l’encodage est de type ASCII, la taille sera de 50% de la taille initiale, sinon 60%
    double tauxCompression = (this->getEncoding() == ASCII ? 0.5 : 0.6);
    return tauxCompression * this->getSize();
}