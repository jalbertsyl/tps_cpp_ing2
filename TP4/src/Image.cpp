/*!
\file Image.cpp
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui défini les fonctions de la classe Image
*/

#include "Image.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur par défaut de Image
*/
Image::Image()
     : width{0}
     , height{0}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur de File
*/
Image::Image(string name, string extension, unsigned int size, unsigned int width, unsigned int height)
     : File(name, extension, size)
     , width{width}
     , height{height}{}

/*!
\fn unsigned int getWidth() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir la largeur de l'image
\return la largeur de l'image
*/
unsigned int Image::getWidth() const{
    return this->width;
}

/*!
\fn void setWidth(unsigned int newWidth)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir une nouvelle largeur d'image
\param newWidth la nouvelle largeur de l'image
*/
void Image::setWidth(unsigned int newWidth){
    this->width = newWidth;
}

/*!
\fn unsigned int getHeight() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir la hauteur de l'image
\return la hauteur de l'image
*/
unsigned int Image::getHeight() const{
    return this->height;
}

/*!
\fn void setHeight(unsigned int newHeight)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir une nouvelle hauteur d'image
\param newHeight la nouvelle hauteur de l'image
*/
void Image::setHeight(unsigned int newHeight){
    this->height = newHeight;
}

/*!
\fn unsigned int getCompressedSize() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet d'avoir la taille du fichier une fois compréssé
\return la taille du fichier une fois compréssé
*/
unsigned int Image::getCompressedSize() const{
    //la taille du fichier compressé sera de la moitié du produit width*height ;
    return this->getWidth() * this->getHeight() / 2;
}