/*!
\file Directory.h
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui déclare la classe Directory
*/

#ifndef __DIRECTORY_H_
#define __DIRECTORY_H_

#include <vector>
#include "File.h"

class Directory {
    private :
        string name;
        vector<File *> files;

    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur par défaut de Directory
        */
        Directory ();

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Constructeur de Directory
        */
        Directory (string name);

        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief Destructeur de Directory
        */
        ~Directory ();

        /*!
        \fn string getName() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir le nom du répertoire
        \return le nom du répertoire
        */
        string getName() const;

        /*!
        \fn void setName(string newName)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de définir un nouveau nom au répertoire
        \param newName le nouveau nom du répertoire
        */
        void setName(string newName);

        /*!
        \fn vector<File *> getFiles() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet de d'obtenir les fichiers présent dans le répertoire
        \return le vecteur de fichiers
        */
        vector<File *> getFiles() const;

        /*!
        \fn void addFile(File * newFile)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 fevrier 2021
        \brief fonction qui permet d'ajouter un fichier au répertoire
        \param newFile le nouveau fichier à ajouter
        */
        void addFile(File * newFile);
    
    /*!
    \fn ostream & operator<<(ostream & out, const Directory & d)
    \author Jalbert Sylvain
    \version 1 Premier jet
    \date 22 janvier 2021
    \brief fonction amie qui permet d'ajouter à une sortie ostream, le Directory sous forme de text
    \param out la sortie ostream de base
    \param d le Directory à ajouter à la sortie
    \return la nouvelle sortie ostream modifié
    */
    friend ostream & operator<<(ostream & out, const Directory & d);
} ;


#endif