/*!
\file File.cpp
\autor Jalbert Sylvain
\version 1
\date 12 fevrier 2021
\brief fichier qui défini les fonctions de la classe File
*/

#include "File.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur par défaut de File
*/
File::File()
    : name{"Sans nom"}
    , extension{".txt"}
    , size{0}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Constructeur de File
*/
File::File(string name, string extension, unsigned int size)
    : name{name}
    , extension{extension}
    , size{size}{}

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief Destructeur de File
*/
File::~File (){}

/*!
\fn string getName() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir le nom du fichier
\return le nom du fichier
*/
string File::getName() const{
    return this->name;
}

/*!
\fn void setName(string newName)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir un nouveau nom au fichier
\param newName le nouveau nom du fichier
*/
void File::setName(string newName){
    this->name = newName;
}

/*!
\fn string getExtension() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir l'extension du fichier
\return l'extension du fichier
*/
string File::getExtension() const{
    return this->extension;
}

/*!
\fn void setExtension(string newExtension)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir une nouvelle extension de fichier
\param newExtension la nouvelle extension du fichier
*/
void File::setExtension(string newExtension){
    this->extension = newExtension;
}

/*!
\fn unsigned int getSize() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de d'obtenir la taille du fichier
\return la taille du fichier
*/
unsigned int File::getSize() const{
    return this->size;
}

/*!
\fn void setSize(unsigned int newSize)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 fevrier 2021
\brief fonction qui permet de définir une nouvelle taille de fichier
\param newSize la nouvelle taille du fichier
*/
void File::setSize(unsigned int newSize){
    this->size = newSize;
}