/*!
\file Time.h
\autor Jalbert Sylvain
\version 1
\date 29 janvier 2021
\brief fichier qui déclare la classe Time
*/

#ifndef __TIME_H_
#define __TIME_H_

#include<string>
#include <iostream>
using namespace std;

class Time {
    private :
        unsigned short hour ;
        unsigned short minute ;
        unsigned short second ;
    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief Constructeur de Time
        \details Initialise les heures, minutes et secondes
        */
        Time (unsigned short hour , unsigned short minute , unsigned short second );

        /*!
        \fn bool operator==(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison "==" de la class Time
        \param other le time qui sera comparé
        \return vrai si les classes sont égales
        */
        bool operator==(const Time & other) const;

        /*!
        \fn bool operator!=(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison "!=" de la class Time
        \param other le time qui sera comparé
        \return vrai si les classes sont différantes
        */
        bool operator!=(const Time & other) const;

        /*!
        \fn bool operator<(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison "<" de la class Time
        \param other le time qui sera comparé
        \return vrai si other est suppérieur
        */
        bool operator<(const Time & other) const;

        /*!
        \fn bool operator>(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison ">" de la class Time
        \param other le time qui sera comparé
        \return vrai si other est inferieur
        */
        bool operator>(const Time & other) const;

        /*!
        \fn bool operator<=(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison "<=" de la class Time
        \param other le time qui sera comparé
        \return vrai si other est suppérieur ou égal
        */
        bool operator<=(const Time & other) const;

        /*!
        \fn bool operator>=(const Time & other) const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de comparaison ">=" de la class Time
        \param other le time qui sera comparé
        \return vrai si other est inférieur ou égal
        */
        bool operator>=(const Time & other) const;

        /*!
        \fn Time & operator++()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur d'incrémentation "++" de la class Time
        \return le nouveau time calculé (this)
        */
        Time & operator++();

        /*!
        \fn Time & operator--()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui permet de définir l'operateur de décrémentation "--" de la class Time
        \return le nouveau time calculé (this)
        */
        Time & operator--();

        /*!
        \fn unsigned short getHour() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui renvoie l'heure
        \return l'heure
        */
        unsigned short getHour() const;

        /*!
        \fn unsigned short getMinute() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui renvoie les minutes
        \return les minutes
        */
        unsigned short getMinute() const;

        /*!
        \fn unsigned short getSecond() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 29 janvier 2021
        \brief fonction qui renvoie les secondes
        \return les secondes
        */
        unsigned short getSecond() const;

    /*!
    \fn ostream & operator<<(ostream & out, const Time & t)
    \author Jalbert Sylvain
    \version 1 Premier jet
    \date 22 janvier 2021
    \brief fonction amie qui permet d'ajouter à une sortie ostream, le Time sous forme de text
    \param out la sortie ostream de base
    \param t le Time à ajouter à la sortie
    \return la nouvelle sortie ostream modifié
    */
    friend ostream & operator<<(ostream & out, const Time & t);
} ;


#endif