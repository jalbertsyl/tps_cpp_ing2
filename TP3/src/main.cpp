/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 29 janvier 2021
\brief fichier principal du programme qui test la class Time
*/

#include "Time.h"

/*!
\fn void afficherTestTime(const Time& premier, const Time& second)
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief procédure qui affiche les comparaisons entre deux Time
*/
void afficherTestTime(const Time& premier, const Time& second){
    if(premier == second)
        cout << "\t  " << premier << "  == " << second << endl;
    if(premier != second)
        cout << "\t  " << premier << "  != " << second << endl;
    if(premier < second)
        cout << "\t  " << premier << "  <  " << second << endl;
    if(premier <= second)
        cout << "\t  " << premier << "  <= " << second << endl;
    if(premier > second)
        cout << "\t  " << premier << "  >  " << second << endl;
    if(premier >= second)
        cout << "\t  " << premier << "  >= " << second << endl;
    cout << endl;
}

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief la fonction principale qui test la class Time
\return 0 si tout c'est bien passé
*/
int main(){
    Time * heureTest = new Time(23, 59, 59);
    Time * heureComparaison = new Time(00, 00, 01);

    // aficher l'état initial des deux heures
    cout << "Initial : " << *heureTest << "  |  " << *heureComparaison << endl;
    afficherTestTime(*heureTest, *heureComparaison);

    // Etape 1 : incrémenter heureTest
    ++(*heureTest);
    cout << "Etape 1 : " << *heureTest << "  |  " << *heureComparaison << endl;
    afficherTestTime(*heureTest, *heureComparaison);

    // Etape 2 : décrémenter heureComparaison
    --(*heureComparaison);
    cout << "Etape 2 : " << *heureTest << "  |  " << *heureComparaison << endl;
    afficherTestTime(*heureTest, *heureComparaison);

    // Etape 3 : incrémenter heureTest
    ++(*heureTest);
    cout << "Etape 3 : " << *heureTest << "  |  " << *heureComparaison << endl;
    afficherTestTime(*heureTest, *heureComparaison);

    // liberer espace mémoire
    delete heureTest;
    delete heureComparaison;

    return(0);
}