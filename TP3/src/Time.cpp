/*!
\file Time.cpp
\autor Jalbert Sylvain
\version 1
\date 29 janvier 2021
\brief fichier qui défini les fonctions de la classe Time
*/

#include "Time.h"

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief Constructeur de Time
\details Initialise les heures, minutes et secondes
*/
Time::Time (unsigned short hour , unsigned short minute , unsigned short second )
    : hour{hour}
    , minute{minute}
    , second{second}{}

/*!
\fn bool operator==(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison "==" de la class Time
\param other le time qui sera comparé
\return vrai si les classes sont égales
*/
bool Time::operator==(const Time & other) const{
    return(this->getHour() == other.getHour() &&
           this->getMinute() == other.getMinute() &&
           this->getSecond() == other.getSecond());
}

/*!
\fn bool operator!=(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison "!=" de la class Time
\param other le time qui sera comparé
\return vrai si les classes sont différantes
*/
bool Time::operator!=(const Time & other) const{
    return(!(*this == other));
}

/*!
\fn bool operator<(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison "<" de la class Time
\param other le time qui sera comparé
\return vrai si other est suppérieur
*/
bool Time::operator<(const Time & other) const{
    bool res = false;
    if(this->getHour() < other.getHour()){
        res = true;
    }else{
        if(this->getHour() == other.getHour()) {
            if(this->getMinute() < other.getMinute()){
                res = true;
            }else{
                if(this->getMinute() == other.getMinute()) {
                    if(this->getSecond() < other.getSecond()){
                        res = true;
                    }
                }
            }
        }
    }
    return(res);
}

/*!
\fn bool operator>(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison ">" de la class Time
\param other le time qui sera comparé
\return vrai si other est inferieur
*/
bool Time::operator>(const Time & other) const{
    return(*this != other && !(*this < other));
}

/*!
\fn bool operator<=(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison "<=" de la class Time
\param other le time qui sera comparé
\return vrai si other est suppérieur ou égal
*/
bool Time::operator<=(const Time & other) const{
    return(*this < other || *this == other);
}

/*!
\fn bool operator>=(const Time & other) const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison ">=" de la class Time
\param other le time qui sera comparé
\return vrai si other est inférieur ou égal
*/
bool Time::operator>=(const Time & other) const{
    return(*this > other || *this == other);
}

/*!
\fn Time & operator++()
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur d'incrémentation "++" de la class Time
\return le nouveau time calculé (this)
*/
Time & Time::operator++(){
    if(this->getSecond() == 59){
        this->second = 0;
        if(this->getMinute() == 59){
            this->minute = 0;
            if(this->getHour() == 23){
                this->hour = 0;
            } else {
                this->hour++;
            }
        } else {
            this->minute++;
        }
    } else {
        this->second = this->getSecond() + 1;
    }
    return(*this);
}

/*!
\fn Time & operator--()
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de décrémentation "--" de la class Time
\return le nouveau time calculé (this)
*/
Time & Time::operator--(){
    if(this->getSecond() == 0){
        this->second = 59;
        if(this->getMinute() == 0){
            this->minute = 59;
            if(this->getHour() == 0){
                this->hour = 23;
            } else {
                this->hour--;
            }
        } else {
            this->minute--;
        }
    } else {
        this->second --;
    }
    return(*this);
}

/*!
\fn unsigned short getHour() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui renvoie l'heure
\return l'heure
*/
unsigned short Time::getHour() const{
    return(this->hour);
}

/*!
\fn unsigned short getMinute() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui renvoie les minutes
\return les minutes
*/
unsigned short Time::getMinute() const{
    return(this->minute);
}

/*!
\fn unsigned short getSecond() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui renvoie les secondes
\return les secondes
*/
unsigned short Time::getSecond() const{
    return(this->second);
}

/*!
\fn ostream & operator<<(ostream & out, const Time & t)
\author Jalbert Sylvain
\version 1 Premier jet
\date 22 janvier 2021
\brief fonction amie qui permet d'ajouter à une sortie ostream, le Time sous forme de text
\param out la sortie ostream de base
\param t le Time à ajouter à la sortie
\return la nouvelle sortie ostream modifié
*/
ostream & operator<<(ostream & out, const Time & t){
    // sous le format "00:00:00"
    return(out << (t.getHour() < 10 ? "0"+to_string(t.getHour()) : to_string(t.getHour())) <<
           ":" << (t.getMinute() < 10 ? "0"+to_string(t.getMinute()) : to_string(t.getMinute())) <<
           ":" << (t.getSecond() < 10 ? "0"+to_string(t.getSecond()) : to_string(t.getSecond())));
}


/*!
\fn bool operator<(const Time &t1 , const Time &t2)
\author Jalbert Sylvain
\version 1 Premier jet
\date 29 janvier 2021
\brief fonction qui permet de définir l'operateur de comparaison "<" entre deux Time
\param t1 le premier Time qui sera comparé
\param t2 le second Time qui sera comparé
\return vrai si t1 est inferieur à t2
*/
/*
bool operator<(const Time &t1 , const Time &t2){
    bool res = false;
    if(t1.getHour() < t2.getHour()){
        res = true;
    }else{
        if(t1.getHour() == t2.getHour()) {
            if(t1.getMinute() < t2.getMinute()){
                res = true;
            }else{
                if(t1.getMinute() == t2.getMinute()) {
                    if(t1.getSecond() < t2.getSecond()){
                        res = true;
                    }
                }
            }
        }
    }
    return(res);
}
*/