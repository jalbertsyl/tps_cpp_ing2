/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 12 janvier 2021
\brief fichier principal du programme qui calcul la somme des Entiers de 0 à N non inclu
*/

#include <vector>
#include <iostream>
using namespace std;

//la taille du vecteur
const int N = 40;

/*!
\fn void sum(int & p, int n, vector<int> d)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief une procédure qui calcul la somme des valeurs d'un vecteur.
\param p la somme calculé
\param n la taille du vecteur
\param d le vecteur
*/
void sum(int & p, int n, vector<int> d) {
    p = 0;
    for(int i = 0 ; i < n ; ++i)
        p = p + d.at(i);
}

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief la fonction principale qui créé un vecteur et appel la procédure sum
\return 0 si tout c'est bien passé
*/
int main(){
    int accum = 0;
    vector<int> data;
    for(int i = 0 ; i < N ; ++i)
        data.push_back(i);
    sum(accum, N, data);
    cout << "sum is " << accum << endl;
    return(0);
}