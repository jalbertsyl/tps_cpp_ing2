/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 12 janvier 2021
\brief fichier principal du programme qui calcul la somme des racines carrés des Entiers de 0 à N non inclu
*/

#include "Vector.hpp"
#include <vector>
#include <iostream>
#include <cmath>
using namespace std;

//la taille du vecteur
const int N = 7;

/*!
\fn void sum_sqrt(Vector& v)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief une fonction qui calcul la somme des racines carrés des valeurs d'un vecteur.
\param v l'objet vecteur
\return la somme calculé
*/
double sum_sqrt(Vector& v){
    double sum = 0 ;
    for(unsigned int i =0 ; i < v.size() ; ++i){
        sum += sqrt(v[i]);
    }
    return(sum);
}

/*!
\fn int main ()
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief la fonction principale qui créé un vecteur et appel la procédure sum_sqrt
\return 0 si tout c'est bien passé
*/
int main(){
    Vector data(N);
    cout << "sum is " << sum_sqrt(data) << endl;
    return(0);
}