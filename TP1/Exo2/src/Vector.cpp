/*!
\file vector.cpp
\autor Jalbert Sylvain
\version 1
\date 12 janvier 2021
\brief fichier qui défini les fonctions de la classe Vector
*/

#include "Vector.hpp"
#include <stdexcept>

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief Constructeur de Vecteur
\details Initialise les éléments
\param s taille du vecteur
*/
Vector::Vector(unsigned int s){
    this->sz = s;
    this->elements = new double[s];
    //initialisation des cases
    for(unsigned int i = 0 ; i < s ; i++){
        this->elements[i] = i;
    }
}
/*!
\fn unsigned int size()
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief fonction qui renvoie la taille du vecteur
\return le nombre d'élément du vecteur
*/
unsigned int Vector::size() const {
    return(this->sz);
}

/*!
\fn double& operator[](unsigned int i)
\author Jalbert Sylvain
\version 1 Premier jet
\date 12 janvier 2021
\brief une fonction qui retourne l'élément du vecteur
\param i l'index de l'élément souhaité du vecteur
\return l'élément
*/
double& Vector::operator[](unsigned int i){
    if(i < this->size()){
        return(elements[i]);
    }
    throw std::out_of_range ("The index is out of range.");
}