/*!
\file main.cpp
\autor Jalbert Sylvain
\version 1
\date 12 janvier 2021
\brief fichier qui déclare la classe Vector
*/

#ifndef __VECTOR_HPP_
#define __VECTOR_HPP_
class Vector {
    private :
        double* elements;
        unsigned int sz;
    public :
        /*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 janvier 2021
        \brief Constructeur de Vecteur
        \details Initialise les éléments
        \param s taille du vecteur
        */
        Vector(unsigned int s);

        /*!
        \fn unsigned int size()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 janvier 2021
        \brief fonction qui renvoie la taille du vecteur
        \return le nombre d'élément du vecteur
        */
        unsigned int size() const;

        /*!
        \fn double& operator[](unsigned int i)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 12 janvier 2021
        \brief une fonction qui retourne l'élément du vecteur
        \param i l'index de l'élément souhaité du vecteur
        \return l'élément
        */
        double & operator[](unsigned int i);
} ;
#endif