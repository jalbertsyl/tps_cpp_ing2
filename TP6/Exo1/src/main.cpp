#include "Stack.hpp"

int main() {
	//création + initialisation de la pile
	Stack<string> myStack;
	myStack.push("42");
	myStack.push("poisson");
	myStack.push("fromage");
	myStack.push("maison");
	myStack.push("rouge");
	myStack.push("Pierre");
	myStack.push("Paul");
	myStack.push("Jaques");

	//dépiler en affichant la pile
	cout << "Voici la pile de " << myStack.size() << " élément(s) :" << endl;
	while(!myStack.empty()){
		cout << "\temlement n°" << myStack.size() << " : " << myStack.peek() << endl;
		myStack.pop();
	}

	return 0;
}
