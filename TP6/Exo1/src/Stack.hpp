/*!
\file Stack.hpp
\autor Jalbert Sylvain
\version 1
\date 05 mars 2021
\brief fichier qui déclare la classe Stack
*/

#ifndef __STACK_H_
#define __STACK_H_

#include <vector>
#include <iostream>
using namespace std;

template <typename T>
class Stack{
	private :
		vector<T> elements;
		unsigned int sz;

	public :
		/*!
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief Constructeur par défaut de Stack
        */
		Stack();
		/*!
        \fn unsigned int size() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief fonction qui retourne la taille de la pile
        \return la taille de la pile
        */
		unsigned int size() const;
		/*!
        \fn bool empty() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief fonction qui indique si la pile est vide
        \return vrai si la pile est vide, faux sinon
        */
		bool empty() const;
		/*!
        \fn T peek() const
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief fonction qui retourne la tête de la pile
        \return la tête de la pile
        */
		T peek() const;
		/*!
        \fn void push(T element)
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief méthode qui permet d’empiler un élément
        */
		void push(T element);
		/*!
        \fn void pop()
        \author Jalbert Sylvain
        \version 1 Premier jet
        \date 05 mars 2021
        \brief méthode qui permet de dépiler
        */
		void pop();
};

/*!
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief Constructeur par défaut de Stack
*/
template<typename T>
Stack<T>::Stack(){
    this->sz = 0;
}
/*!
\fn unsigned int size() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief fonction qui retourne la taille de la pile
\return la taille de la pile
*/
template<typename T>
unsigned int Stack<T>::size() const{
    return this->sz;
}
/*!
\fn bool empty() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief fonction qui indique si la pile est vide
\return vrai si la pile est vide, faux sinon
*/
template<typename T>
bool Stack<T>::empty() const{
    return(this->sz == 0);
}
/*!
\fn T peek() const
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief fonction qui retourne la tête de la pile
\return la tête de la pile
*/
template<typename T>
T Stack<T>::peek() const{
    return this->elements.back();
}
/*!
\fn void push(T element)
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief méthode qui permet d’empiler un élément
*/
template<typename T>
void Stack<T>::push(T element){
    this->elements.push_back(element);
    this->sz++;
}
/*!
\fn void pop()
\author Jalbert Sylvain
\version 1 Premier jet
\date 05 mars 2021
\brief méthode qui permet de dépiler
*/
template<typename T>
void Stack<T>::pop(){
    this->elements.pop_back();
    this->sz--;
}

#endif
